# Gilded Rose Kata

My take on the [Gilded Rose Kata](https://github.com/emilybache/GildedRose-Refactoring-Kata).

Was more fun that I expected.

Decided to change Item class and take on the Goblin. Let's hope my evasion stats are high enough.
