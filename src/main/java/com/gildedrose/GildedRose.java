package com.gildedrose;

import java.util.List;

class GildedRose {

    private static final int MAX_QUALITY = 50;

    static void updateQuality(List<Item> items) {
        for (Item item : items) {
            if (item.isQualityCanDecrease() && !item.isUselessAfterSellIn()) {
                decreaseQuality(item);
            } else {
                increaseQuality(item);

                if (item.isUselessAfterSellIn()) {
                    if (item.getSellIn() < 11) {
                        increaseQuality(item);
                    }

                    if (item.getSellIn() < 6) {
                        increaseQuality(item);
                    }
                }
            }
            decreaseSellIn(item);

            if (item.getSellIn() < 0) {
                if (item.isQualityCanDecrease()) {
                    if (item.isUselessAfterSellIn()) {
                        resetQuality(item);
                    } else {
                        decreaseQuality(item);
                    }
                } else {
                    increaseQuality(item);
                }
            }
        }
    }

    private static void decreaseSellIn(Item item) {
        if (item.isSellInCanDecrease()) {
            item.setSellIn(item.getSellIn() - 1);
        }
    }

    private static void decreaseQuality(Item item) {
        if (item.getQuality() > 0 && item.isQualityCanDecrease()) {
            item.setQuality(item.getQuality() - item.getQualityDecreaseCoefficient());

            if (item.getQuality() < 0) {
                resetQuality(item);
            }
        }
    }

    private static void increaseQuality(Item item) {
        if (item.getQuality() < MAX_QUALITY) {
            item.setQuality(item.getQuality() + 1);
        }
    }

    private static void resetQuality(Item item) {
        item.setQuality(0);
    }
}
