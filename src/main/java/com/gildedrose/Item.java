package com.gildedrose;

public class Item {

    private String name;

    private int sellIn;

    private int quality;

    private int qualityDecreaseCoefficient;

    private boolean qualityCanDecrease;

    private boolean sellInCanDecrease;

    private boolean uselessAfterSellIn;

    Item(String name, int sellIn, int quality, int qualityDecreaseCoefficient, boolean qualityCanDecrease, boolean sellInCanDecrease, boolean uselessAfterSellIn) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
        this.qualityDecreaseCoefficient = qualityDecreaseCoefficient;
        this.qualityCanDecrease = qualityCanDecrease;
        this.sellInCanDecrease = sellInCanDecrease;
        this.uselessAfterSellIn = uselessAfterSellIn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSellIn() {
        return sellIn;
    }

    public void setSellIn(int sellIn) {
        this.sellIn = sellIn;
    }

    public int getQuality() {
        return quality;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public boolean isQualityCanDecrease() {
        return qualityCanDecrease;
    }

    public void setQualityCanDecrease(boolean qualityCanDecrease) {
        this.qualityCanDecrease = qualityCanDecrease;
    }

    public int getQualityDecreaseCoefficient() {
        return qualityDecreaseCoefficient;
    }

    public void setQualityDecreaseCoefficient(int qualityDecreaseCoefficient) {
        this.qualityDecreaseCoefficient = qualityDecreaseCoefficient;
    }

    public boolean isSellInCanDecrease() {
        return sellInCanDecrease;
    }

    public void setSellInCanDecrease(boolean sellInCanDecrease) {
        this.sellInCanDecrease = sellInCanDecrease;
    }

    public boolean isUselessAfterSellIn() {
        return uselessAfterSellIn;
    }

    public void setUselessAfterSellIn(boolean uselessAfterSellIn) {
        this.uselessAfterSellIn = uselessAfterSellIn;
    }

    @Override
   public String toString() {
        return this.name + ", " + this.sellIn + ", " + this.quality;
    }
}
