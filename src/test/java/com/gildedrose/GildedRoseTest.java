package com.gildedrose;

import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GildedRoseTest {

    @Test
    public void foo() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("foo", 0, 0, 1, true, true, false));
        GildedRose.updateQuality(items);
        assertEquals("foo", items.get(0).getName());
    }

    @Test
    public void checkIfEverythingWorks() throws IOException {
        String standard = new String(Files.readAllBytes(Paths.get("src/test/resources/standard.txt")));
        String current = TextTestFixture.buildStandard().toString();
        assertEquals(standard, current);
    }

    @Test
    public void qualityNeverHigherThan50() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("Aged Brie", 2, 50, 1, false, true, false));
        items.add(new Item("Backstage passes to a TAFKAL80ETC concert", 5, 50, 1, true, true, true));

        GildedRose.updateQuality(items);
        assertEquals(50, items.get(0).getQuality());
        assertEquals(50, items.get(1).getQuality());
    }

    @Test
    public void qualityIncreasesForAgedBrie() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("Aged Brie", 2, 25, 1, false, true, false));

        GildedRose.updateQuality(items);
        assertEquals(26, items.get(0).getQuality());
    }

    @Test
    public void qualityNeverNegative() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("foo", 2, 0, 1, true, true, false));
        items.add(new Item("Conjured Mana Cake", 5, 1, 2, true, true, false));

        GildedRose.updateQuality(items);
        assertEquals(0, items.get(0).getQuality());
        assertEquals(0, items.get(1).getQuality());
    }

    @Test
    public void qualityDecreasesByOneForUsualItems() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("foo", 2, 1, 1, true, true, false));

        GildedRose.updateQuality(items);
        assertEquals(0, items.get(0).getQuality());
    }


    @Test
    public void qualityIs80ForSulfuras() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("Sulfuras, Hand of Ragnaros", 0, 80, 1, false, false, false));
        items.add(new Item("Sulfuras, Hand of Ragnaros", -1, 80, 1, false, false, false));

        GildedRose.updateQuality(items);
        assertEquals(80, items.get(0).getQuality());
        assertEquals(80, items.get(1).getQuality());
    }

    @Test
    public void qualityDecreasesTwiceAsFastAfterSellInPassed() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("foo", 1, 10, 1, true, true, false));

        GildedRose.updateQuality(items);
        assertEquals(9, items.get(0).getQuality());
        GildedRose.updateQuality(items);
        assertEquals(7, items.get(0).getQuality());
    }

    @Test
    public void sellInDecreasesAfterUpdate() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("foo", 10, 10, 1, true, true, false));

        GildedRose.updateQuality(items);
        assertEquals(9, items.get(0).getSellIn());
    }

    @Test
    public void backstageIncreaseInQualityByOne() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20, 1, true, true, true));

        GildedRose.updateQuality(items);
        assertEquals(21, items.get(0).getQuality());
    }

    @Test
    public void backstageQualityDropsToZeroAfterSellInPasses() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("Backstage passes to a TAFKAL80ETC concert", 0, 20, 1, true, true, true));

        GildedRose.updateQuality(items);
        assertEquals(0, items.get(0).getQuality());
    }

    @Test
    public void backstageIncreaseInQualityByTwoWith10DaysOrLess() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("Backstage passes to a TAFKAL80ETC concert", 10, 20, 1, true, true, true));

        GildedRose.updateQuality(items);
        assertEquals(22, items.get(0).getQuality());
        GildedRose.updateQuality(items);
        assertEquals(24, items.get(0).getQuality());
    }

    @Test
    public void backstageIncreaseInQualityByThreeWith5DaysOrLess() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("Backstage passes to a TAFKAL80ETC concert", 5, 20, 1, true, true, true));

        GildedRose.updateQuality(items);
        assertEquals(23, items.get(0).getQuality());
        GildedRose.updateQuality(items);
        assertEquals(26, items.get(0).getQuality());
    }

    @Test
    public void conjuredItemsDegradesTwiceAsFast() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("Conjured Mana Cake", 5, 20, 2, true, true, false));

        GildedRose.updateQuality(items);
        assertEquals(18, items.get(0).getQuality());
    }

    @Test
    public void conjuredItemsDegradationQuadruplesAfterSellInPassed() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("Conjured Mana Cake", 0, 20, 2, true, true, false));

        GildedRose.updateQuality(items);
        assertEquals(16, items.get(0).getQuality());
    }
}
