package com.gildedrose;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class TextTestFixture {

    public static void main(String[] args) throws Exception {
        StringBuilder builder = buildStandard();
        Files.write(Paths.get("src/test/resources/standard.txt"), builder.toString().getBytes());
    }

    static StringBuilder buildStandard() {
        List<Item> items = new LinkedList<>();
        items.add(new Item("+5 Dexterity Vest", 10, 20, 1, true, true, false));
        items.add(new Item("Aged Brie", 2, 0, 1, false, true, false));
        items.add(new Item("Elixir of the Mongoose", 5, 7, 1, true, true, false));
        items.add(new Item("Sulfuras, Hand of Ragnaros", 0, 80, 1, false, false, false));
        items.add(new Item("Sulfuras, Hand of Ragnaros", -1, 80, 1, false, false, false));
        items.add(new Item("Backstage passes to a TAFKAL80ETC concert", 15, 20, 1, true, true, true));
        items.add(new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49, 1, true, true, true));
        items.add(new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49, 1, true, true, true));
        items.add(new Item("Conjured Mana Cake", 3, 6, 2, true, true, false));

        StringBuilder standard = new StringBuilder();

        int days = 50;

        for (int i = 0; i < days; i++) {
            standard.append("-------- day ").append(i).append(" --------");
            standard.append("name, sellIn, quality");
            for (Item item : items) {
                standard.append(item);
            }
            standard.append("\n");
            GildedRose.updateQuality(items);
        }

        return standard;
    }

}
